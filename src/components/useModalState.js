import { ref } from "vue";
export default function () {
  const modalOpen = ref(false);
  const toggleModalState = () => {
    modalOpen.value = !modalOpen.value;
  };
  return { modalOpen, toggleModalState }
};
